#include "LevelScreen.h"
#include "Game.h"

LevelScreen::LevelScreen(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, platformInstance()
	, platformInstance1()
	, camera(newGamePointer->GetWindow().getDefaultView())
{
	// Calculate center of the screen
	sf::Vector2f newPosition;

	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	// Calculate position of the platform to be centered
	newPosition.x -= platformInstance.GetHitBox().width / 2;
	newPosition.y -= platformInstance.GetHitBox().height / 2;

	// Add to the y position to lower the platform a bit
	const float PLATFORM_OFFSET = 250;

	newPosition.y += PLATFORM_OFFSET;

	// Set the new position of the platform
	platformInstance.SetPosition(newPosition);

	newPosition.y -= 200;

	platformInstance1.SetPosition(newPosition);
}

void LevelScreen::Input()
{
	playerInstance.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);
	playerInstance.HandleSolidCollision(platformInstance.GetHitBox());
	playerInstance.HandleSolidCollision(platformInstance1.GetHitBox());
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	// Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	float playerCenterY = playerInstance.GetHitBox().top + playerInstance.GetHitBox().height / 2;

	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}

	// Set camera view
	target.setView(camera);

	// Draw content to screen
	playerInstance.DrawTo(target);
	platformInstance.DrawTo(target);
	platformInstance1.DrawTo(target);

	// Remove camera view
	target.setView(target.getDefaultView());
}
