#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"

class Game;

class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

private:
	Player playerInstance;
	Game* gamePointer;
	Platform platformInstance;
	Platform platformInstance1;
	sf::View camera;
};

