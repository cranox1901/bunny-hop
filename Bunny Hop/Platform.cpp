#include "Platform.h"
#include "AssetManager.h"

Platform::Platform()
	: SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Platform.png"))
{
}

void Platform::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}
